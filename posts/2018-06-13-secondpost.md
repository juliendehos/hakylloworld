---
title: My second post
---

This is my second post.

# Image

![](images/haskell.svg)

# Section

This is a section.

## Subsection

This is a subsection.

## Source code

```haskell
main = print 42
```

