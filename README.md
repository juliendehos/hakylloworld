# Hakylloworld

 [![pipeline status](https://gitlab.com/juliendehos/hakylloworld/badges/master/build.svg)](https://gitlab.com/juliendehos/hakylloworld/pipelines) 

A sample web site using [Hakyll](https://jaspervdj.be/hakyll/).

<http://juliendehos.gitlab.io/hakylloworld>


## Build with Nix

```bash
nix-shell --run "cabal run hakylloworld build"
```

then see `public/index.html`


## Build with Stack

```bash
stack setup
stack build
stack exec hakylloworld build
```

then see `public/index.html`

