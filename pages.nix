{ pkgs ? import <nixpkgs> {} }:
let
  app = import ./default.nix { inherit (pkgs); };
in
  pkgs.stdenv.mkDerivation {
    name = "hakylloworld-pages";
    src = ./.;
    buildPhase = ''
      rm -rf _cache dist public result
      ${app}/bin/hakylloworld build
    '';
    installPhase = ''
      mkdir -p $out/public
      cp -R public/* $out/public/
    '';
  }

